# Fiche d'installation

## Dépendances

### NodeJs

Le logiciel utilise NodeJs LTS version 14.17.3 (disponible ici https://nodejs.org/en/)

### Base de données

Le logiciel utilise PostgreSQL 13.3 comme gestionnaire de base de données (disponible ici https://www.postgresql.org/download/).


## Installation

```
# copie du dépôt Git
git clone https://gitlab.utc.fr/goddejer/recueil.git
# racine du projet
cd ./recueil
# installation des modules client
cd ./client
npm install
# puis installation des modules server
cd ../server
npm install
```

## Configuration du serveur

### Ajout du fichier /server/app/config/db.config.js

```js
export default {
    HOST: "localhost",     // adresse du serveur où est située la base de données
    PORT: 5342,            // port configuré sur ce serveur pour la base de données
    USER: "postgres",      // nom de l'utilisateur (mieux vaut ne pas utiliser le superuser)
    PASS: "...",           // mot de passe associé à cet utilisateur
    NAME: "database",      // nom de la base de données
    dialect: "postgres",   // dialect ("postgres" est la valeur pour PostgreSQL)
    pool: {
        max: 50,
        min: 0,
        acquire: 30000,
        idle: 10000,
    }
}
```

### Ajout du fichier /server/app/config/auth.config.js

Ce fichier contient les identifiants que les requêtes clientes doivent fournir à l'API afin d'être authentifiées. Il contient également la license permettant de s'assurer que la version de l'application client est compatible avec la version de l'application serveur ainsi que de prévenir les attaques CSRF. Enfin il est nécessaire de renseigner le fichier contenant la clef privée servant à signer et à authentifier les données sensibles du server. Le répertoire racine utilisé est le dossier `/server/app/config/cert`

```js
export default {
    name: '...',
    pass: '...',
    LICENCE: 'license recueil EMERAUDE (CC BY-SA 2.0) by Jérémy Godde',
    PRIV_KEY_URL: '/priv_key.key'
}
```

### Ajout du dossier /server/app/config/cert

N'oubliez pas d'ajouter la clef privée du server et d'être cohérent entre le nom du fichier et le nom renseigné dans la configuration

### Ajout du fichier /client/auth.config.ts

Ce fichier contient la configuration nécessaire pour contacter le serveur backend détenant l'API et le token d'authentification de votre client qui correspond au `credentials` de la documention officielle https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization.
De plus, dans la partie `Decryption for Sessions`, copiez le contenu de votre clef publique et n'oubliez pas de remplacer les sauts de lignes par le character `\n`

```ts
export default {
    //Authentification API
    HOST: 'localhost',     // adresse du serveur backend
    PORT: 8080,            // port configuré sur ce serveur backend pour l'écoute de l'API
    URL_ROOT: '/api/v0',   // racine de l'url de l'API (ici: http://localhost:8080/api/v0/...)
    token: '...',          // Basic credentials
    //Decryption for Sessions
    publ_key: '-----BEGIN PUBLIC KEY-----\n...\n-----END PUBLIC KEY-----'
}
```
