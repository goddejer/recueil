# Recueil (WIP)

## Organisation du dépôt Git

### Issues

#### Convention de création des labels

Il y a deux boards principaux `Development` et `Debug` qui permettent de suivre l'avancée globale du projet.

Ces boards utilisent respectivement les labels suivants pour marquer la progression des tâches : `to add`, `priority to add`, `wip`, `ready`, `merged` et `request bug`, `wip bug`, `ready bug`, `merged bug`.

Ensuite il y a les `labels de groupe` qui regroupent des fonctionnalités proches (par exemple toutes les fonctionnalités liés à la page utilisateur). Seuls ceux ci peuvent être créés en utilisant un **titre clair et concis** (par exemple *page utilisateur*), en renseignant une **description du groupe de fonctionnalité** (*présentation et modification des informations utilisateurs et présentation de ces oeuvres*) et en choisissant une couleur.

#### Convention de création des issues

##### Titre

```
(<label de groupe>_<numéro d'issue>) <titre court>
```

Exemple pour la 8ème issue du label de groupe `page utilisateur` portant sur l'ajout d'un formulaire de modification des informations de l'utilisateur : `(page utilisateur_8) ajout formulaire de modification`.

##### Description

La description doit expliquer clairement ce qu'il doit être réaliser. Elle doit regrouper toutes les informations nécessaires à la mise en place d'une fonctionnalité ou toutes les informations permettant de traquer un bug.

##### Attribution

Il n'est pas nécessaire de s'attribuer l'issue à sa création, mais il est nécessaire de le faire avant de commencer à travailler dessus.

##### Labels

Il est important de marquer l'issue avec un label de progression et un label de groupe.

##### Autres champs

Les autres champs ne sont pas utilisés.

##### Label de progression

Lorsqu'on travaille sur une nouvelle issue, ne pas oublier de la déplacer sur le board correspondant afin que sa progression soit à jour.

### Convention des commit

```
<issue> <court descriptif>
# Exemple: page utilisateur_8 suppression champ age
```

### Branches

#### Convention de création de branches

```
# Les branches de production :
prod_<version>
# exemple: prod_1.1.3
# Les branches principales de développement :
dev_<version>_init
# exemple: dev_1.2_init
# Les branches de développement de fonctionnalités :
dev_<version>_<issue>
# exemple: dev_1.2_page_utilisateur_8
```

#### Règle de merges

##### Attribution

##### Labels

##### Explicatif du merge

## Présentation du projet