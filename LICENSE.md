# Licence du logiciel Recueil

Auteur: Jérémy GODDE

Version du logiciel: 0.0

Version de la licence: 1.0

Ce logiciel est mise à disposition sous licence Attribution -  Partage dans les Mêmes Conditions 2.0 France. Pour voir une copie de cette licence, visitez http://creativecommons.org/licenses/by-sa/2.0/fr/ ou écrivez à Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

![licence Attribution -  Partage dans les Mêmes Conditions 2.0 France](https://licensebuttons.net/l/by-sa/2.0/fr/88x31.png)